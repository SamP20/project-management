import argparse
import os
import urllib.request
import json
import subprocess
from string import Template
import shutil
import sys
import webbrowser

def main():
    parser = argparse.ArgumentParser()
    subparsers = parser.add_subparsers(title="commands")
    _parser_new = NewCommand(subparsers)
    _parser_sync = SyncCommand(subparsers)
    _parser_clone = CloneCommand(subparsers)
    _parser_info = InfoCommand(subparsers)
    _parser_open = OpenCommand(subparsers)
    _parser_search = SearchCommand(subparsers)

    args = parser.parse_args()
    args.command(args)



class NewCommand:
    def __init__(self, subparsers: argparse.ArgumentParser):
        self.parser = subparsers.add_parser("new", help="create new project")
        self.parser.add_argument("-t", "--template", help="name of template to use")
        self.parser.add_argument("-k", "--keywords", nargs="+", help="list of tags for the project")
        self.parser.add_argument("--visibility", choices=["private", "internal", "public"], default="private", help="name of template to use")
        self.parser.add_argument("name", help="name of project")
        self.parser.add_argument("description", nargs="?", help="description of project")
        self.parser.set_defaults(command=self.command)

    def command(self, args):
        result = api_post("/projects", name=args.name, description=args.description, tag_list=args.keywords, visibility=args.visibility)
        ssh_url = result["ssh_url_to_repo"]
        path = result["path_with_namespace"]
        fullpath = os.path.join(os.environ["PROJECTS_PATH"], path)

        if args.template is None:
            subprocess.run(["git", "clone", ssh_url, fullpath], check=True)
        else:
            namespace = result["namespace"]["full_path"]
            project_uri = f"{namespace}/template-{args.template}"
            template_result = api_get(f"/projects/{urllib.parse.quote_plus(project_uri)}")
            template_ssh_url = template_result["ssh_url_to_repo"]
            subprocess.run(["git", "clone", template_ssh_url, fullpath], check=True)

            old_cwd = os.getcwd()
            sys.path.append(fullpath)
            os.chdir(fullpath)
            subprocess.run(["git", "remote", "set-url", "origin", ssh_url], check=True)
            # pylint: disable=import-error
            import TEMPLATE_SETUP as tpl_setup
            tpl_setup.initialize(result)
            subprocess.run(["git", "rm", "TEMPLATE_SETUP.py"], check=True)
            shutil.rmtree("__pycache__") # Remove cached TEMPLATE_SETUP.pyc
            subprocess.run(["git", "add", "-A"], check=True)
            subprocess.run(["git", "commit", "-m", "setup template"], check=True)
            subprocess.run(["git", "clean", "-f"], check=True)
            subprocess.run(["git", "push", "-u", "origin", "master"], check=True)
            os.chdir(old_cwd)


class SyncCommand:
    def __init__(self, subparsers: argparse.ArgumentParser):
        self.parser = subparsers.add_parser("sync", help="synchronizes all projects")
        self.parser.add_argument("--user", help="owner of project")
        self.parser.set_defaults(command=self.command)

    def command(self, args):
        if args.user is None:
            args.user = api_get(f"/user")["username"]

        projects = api_get(f"/users/{urllib.parse.quote_plus(args.user)}/projects")
        for project in projects:
            path = project["path_with_namespace"]
            fullpath = os.path.join(os.environ["GITLAB_BACKUP_PATH"], path)
            ssh_url = project["ssh_url_to_repo"]
            if os.path.exists(fullpath):
                old_cwd = os.getcwd()
                os.chdir(fullpath)
                subprocess.run(["git", "remote", "update", "--prune"], check=True)
                print(f"updated {path}")
                os.chdir(old_cwd)
            else:
                subprocess.run(["git", "clone", "--mirror", ssh_url, fullpath], check=True)
                print(f"created new repo {path}")


class CloneCommand:
    def __init__(self, subparsers: argparse.ArgumentParser):
        self.parser = subparsers.add_parser("clone", help="clone a project")
        self.parser.add_argument("--user", help="owner of project")
        self.parser.add_argument("name", help="name of project")
        self.parser.set_defaults(command=self.command)

    def command(self, args):
        if args.user is None:
            args.user = api_get(f"/user")["username"]

        project_uri = f"{args.user}/{args.name}"
        project = api_get(f"/projects/{urllib.parse.quote_plus(project_uri)}")

        ssh_url = project["ssh_url_to_repo"]
        path = project["path_with_namespace"]
        fullpath = os.path.join(os.environ["PROJECTS_PATH"], path)

        subprocess.run(["git", "clone", ssh_url, fullpath], check=True)


class InfoCommand:
    def __init__(self, subparsers: argparse.ArgumentParser):
        self.parser = subparsers.add_parser("info", help="display details of a project")
        self.parser.add_argument("--user", help="owner of project")
        self.parser.add_argument("name", help="name of project")
        self.parser.set_defaults(command=self.command)

    def command(self, args):
        if args.user is None:
            args.user = api_get(f"/user")["username"]

        project_uri = f"{args.user}/{args.name}"
        project = api_get(f"/projects/{urllib.parse.quote_plus(project_uri)}")
        print(project["path_with_namespace"])
        print(f"  {project['description']}\n")
        print(f"owner: {project['owner']['name']}({project['owner']['username']})")
        print(f"web url: {project['web_url']}")
        print(f"ssh url: {project['ssh_url_to_repo']}")
        print(f"tags: {','.join(project['tag_list'])}")


class OpenCommand:
    def __init__(self, subparsers: argparse.ArgumentParser):
        self.parser = subparsers.add_parser("open", help="opens the gitlab page for the project")
        self.parser.add_argument("--user", help="owner of project")
        self.parser.add_argument("name", help="name of project")
        self.parser.set_defaults(command=self.command)

    def command(self, args):
        if args.user is None:
            args.user = api_get(f"/user")["username"]

        project_uri = f"{args.user}/{args.name}"
        project = api_get(f"/projects/{urllib.parse.quote_plus(project_uri)}")
        webbrowser.open_new_tab(project['web_url'])


class SearchCommand:
    def __init__(self, subparsers: argparse.ArgumentParser):
        self.parser = subparsers.add_parser("search", help="search for projects")
        self.parser.add_argument("--user", help="owner of project")
        self.parser.add_argument("search", nargs="*", help="search critera")
        self.parser.set_defaults(command=self.command)

    def command(self, args):
        if args.user is None:
            args.user = api_get(f"/user")["username"]
        projects = api_get(f"/users/{urllib.parse.quote_plus(args.user)}/projects", search=" ".join(args.search))
        for project in projects:
            print(project["path_with_namespace"])
            print(f"  {project['description']}\n")


def api_get(endpoint, **kwargs):
    return api_call(endpoint, "GET", kwargs)


def api_post(endpoint, **kwargs):
    return api_call(endpoint, "POST", kwargs)


def api_call(endpoint, method, kwargs=None):
    url = f"https://gitlab.com/api/v4{endpoint}"

    data = None
    if kwargs:
        data = recursive_urlencode(kwargs).encode("ascii")

    req = urllib.request.Request(url, headers={
        "Private-Token": os.environ["GITLAB_ACCESS_TOKEN"]
    }, data=data, method=method)

    return json.load(urllib.request.urlopen(req))


def recursive_urlencode(data):
    def r_urlencode(data, parent: str, pairs: list):
        if isinstance(data, list):
            p = parent + "[]"
            for v in data:
                r_urlencode(v, p, pairs)
        elif isinstance(data, dict):
            for k, v in data:
                p = parent + f"[{k}]"
                r_urlencode(v, p, pairs)
        elif data is not None:
            pairs.append((parent, data))
    pairs = []
    for k, v in data.items():
        r_urlencode(v, k, pairs)
    return urllib.parse.urlencode(pairs)