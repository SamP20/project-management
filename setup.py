import setuptools

with open("README.md", "r") as fh:
    long_description = fh.read()

setuptools.setup(
    name="project-management",
    version="0.0.3",
    author="Sam Partridge",
    description="A tool to manage my projects using the Gitlab API.",
    long_description=long_description,
    long_description_content_type="text/markdown",
    url="https://gitlab.com/SamP20/project-management",
    packages=setuptools.find_packages("src"),
    package_dir={"": "src"},
    classifiers=[
        "Programming Language :: Python :: 3",
        "License :: OSI Approved :: MIT License",
        "Operating System :: OS Independent",
    ],
    entry_points={
        "console_scripts": [
            "proj=project_management:main"
        ]
    }
)